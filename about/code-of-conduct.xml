<?xml version='1.0'?>

<!DOCTYPE cscpage SYSTEM "../csc.dtd">

<cscpage title="Code of Conduct for the Computer Science Club of the University of Waterloo">

<header />

<section title="Code of Conduct" anchor="title">
</section>

<section title="Purpose" anchor="purpose">
  <p>
    One of the primary goals of the Computer Science Club of the University
    of Waterloo is the inclusion and support of all members of the University
    of Waterloo community who are interested in Computer Science. As such, we
    are committed to providing a friendly, safe and welcoming environment for
    all, regardless of gender, sex, sexual orientation, ability, ethnicity,
    socioeconomic status, age, and religion or lack thereof.
  </p>
  <p>
    We invite all those who participate in our events and who communicate with
    our Club at large to help us create a safe and positive experience for
    everyone involved.
  </p>
  <p>
    We've outlined a code of conduct to highlight our expectations for all
    individuals who participate in our Club, as well as the steps to
    handle unacceptable behaviour. New members should sign the Code of Conduct.
  </p>
  <p>
    The Code of Conduct is in addition to existing University Policies,
    such as policies 33, 34, and 42.
  </p>
  <p>
    The Code of Conduct does not cover criminal matters. Initiating a Code of
    Conduct complaint does not exclude other paths, such as going to Police
    Services. For criminal matters, threats or acts of physical violence,
    immediately contact UW Police at x22222 or (519) 888-4911.
  </p>
</section>

<section title="Expected Behaviour" anchor="expected-behaviour">
  <ul>
    <li>
      Participate in an authentic and active way. In doing so, you contribute
      to the health and longevity of this Club.
    </li>
    <li>
      Exercise consideration and respect in your speech and actions.
    </li>
    <li>
      Attempt collaboration before conflict.
    </li>
    <li>
      Refrain from demeaning, discriminatory, or harassing behaviour and speech.
    </li>
    <li>
      Be mindful of your surroundings and of your fellow participants.
    </li>
  </ul>
</section>

<section title="Unacceptable Behaviour" anchor="unacceptable">
  <p>
    <strong>Unacceptable behaviours include</strong>: intimidating,
    harassing, abusive, discriminatory, derogatory or demeaning
    speech or actions by any participant in our community online, at all
    related events and in one-on-one communications carried out in the
    context of Club business. Club event venues may be shared; please be
    respectful to all patrons of these locations.
  </p>
  <p>
    <strong>Harassment includes</strong>: harmful or prejudicial verbal or
    written comments related to gender, sexual orientation, race, religion,
    disability; inappropriate use of nudity and/or sexual images in public
    spaces (including presentation slides); deliberate intimidation, stalking
    or following; harassing photography or recording; sustained disruption of
    talks or other events; inappropriate physical contact, and unwelcome
    sexual attention.
  </p>
  <p>Also refer to Policy 33 for the definitions of discrimination and
  harassment.</p>

</section>

<section title="Experiencing Unacceptable Behaviour" anchor="experiencing">
  <p>
    The Executive Council and Faculty Advisor are herein referred to as the
    Officers, or singularly as Officer.
  </p>
  <p>
    If you notice a dangerous situation, someone in distress, or violations of
    this Code of Conduct, <a href="../about/">contact an Officer</a>. No
    situation is considered inconsequential. If you do not feel comfortable
    contacting an Executive Council member due to the nature of the incident,
    you may contact the <a href="../about/exec#advisor">Faculty Advisor</a>.
  </p>
  <p>
    Upon receiving a complaint the Officer will inform the first of the
    following people who is not personally involved in the situation, or in a
    close relationship with the someone involved in the situation and is
    available, and this person shall handle the complaint and shall here after
    be referred to as the Handling Officer.
    <ol>
      <li>
        The President
      </li>
      <li>
        The Vice President
      </li>
      <li>
        Any other Executive Council Member
      </li>
      <li>
        The Faculty Advisor
      </li>
    </ol>
  </p>
  <p>
    The Handling Officer will interview the subject of the complaint and any
    witnesses and consult with other relevant Officers. The Handling Officer
    shall chair a handling committee of the faculty advisor and one other
    officer chosen in the same way. This committee shall be familiar with
    University Policies 33, 34, and 42.
  </p>
  <p>
    The Faculty advisor will make sure
    that all applicable University policies, laws, and bylaws are followed. The
    Faculty advisor must always be notified of all complaints and decisions.
  </p>
</section>

<section title="Consequences of Unacceptable Behaviour" anchor="consequences">
  <p>
    After having done so, the Handling Officer shall use their best judgment to
    determine if the complaint is valid and, if so, determine with the relevant
    Officers the appropriate action to ensure that the complainant feels
    welcome in the Computer Science Club and to avoid a subsequent incident:
    <ul>
      <li>
        A warning.
      </li>
      <li>
        A suspension from the events and spaces governed by the Code of Conduct
        until the beginning of the next term. If the suspension would come into
        effect less than two full weeks from the end of classes in the current
        term, then the suspension applies to the subsequent term as well.
      </li>
      <li>
        If the incident is very serious, or the subject has a pattern of
        similar offences, expulsion from the Club.
      </li>
      <li>
        A formal complaint through University policy, such as 33, 34, and 42.
      </li>
    </ul>
  </p>
  <p>
    The Handling Officer shall inform the complainant of the resolution of the
    issue and inform both the complainant and the subject of their right to
    appeal the decision.
  </p>
</section>

<section title="Addressing Grievances" anchor="grievances">
  <p>
    If either the complainant or the subject disagree with the decision made by
    the Handling Officer, they can appeal to the Officers, who can overturn the
    decision with a majority vote of all the Officers.
  </p>
  <p>
    No Officer who was personally involved in the complaint, or is in a close
    relationship with someone involved in the complaint, shall participate in
    the Officers deliberation or vote on the appeal.
  </p>
  <p>
    If the subject of a complaint is expelled from the Club, then at their
    request, but no more often than once a year, the Officers will review the
    decision to expel them from the Club. The Officers can reinstate the
    subject with a two-thirds vote of the Officers.
  </p>
</section>

<section title="Confidentiality" anchor="confidential">
  <p>
    The Club recognizes that all members have a right to privacy, and will
    handle complaints confidentially.
  </p>
  <p>
  As such, proceedings will be kept confidential as described below, to the
  extent allowed by whichever University policy applies to the complaint.
  Relevant policies include
  Policy 42 (Prevention and Response to Sexual Violence),
  Policy 33 (Ethical Behaviour),
  or Policy 34 (Health, Safety, and Environment).
  </p>
  <p>
  Information will only be reported to Police Services when directly required
  to by applicable policy. In such a case, only the required information will
  be provided, anonymized if possible.
  </p>
  <p>
    <strong>Information that will be kept in Club records and be available to
    Officers in the future will be limited to</strong>: the date the complaint
    was made, the name of the subject of the complaint, the name of the
    Handling Officer, the decision made on the complaint, the date the decision
    on the complaint was made, and if applicable, the date of the appeal, the
    party making the appeal (Complainant or Subject), the decision made on the
    appeal by the Officers, and the date of the Officers decision on the appeal.
  </p>
  <p>
    <strong>The information the Handling Officer and Faculty Advisor will
    jointly keep records of, in addition to the information kept in the Club
    records, will be limited to </strong>: the name of the complainant, and a
    summary of the complaint.  This information will be available to Officers
    that are handling future complaints if it is requested and the Handling
    Officer deems it relevant to the new complaint.
  </p>
</section>

<section title="Scope and Spaces" anchor="scope">
  <p>
    In cases where the Code of Conduct contradicts University policies, or
    applicable laws and bylaws, the Code of Conduct does not apply to the extent
    to which it conflicts.
  </p>
  <p>
    We expect all Club participants (participants, organizers, sponsors, and
    other guests) to abide by this Code of Conduct in all community venues
    (online and in-person) as well as in all one-on-one communications
    pertaining to Club business.
  </p>
  <ul>
    <li>
      The Code of Conduct applies in the office, where office staff are
      responsible for enforcing it.
    </li>
    <li>
      The Code of Conduct applies in the IRC channel, where channel operators
      are responsible for enforcing it.
    </li>
    <li>
      The Code of Conduct applies at events the CSC organizes or co-organizes,
      where a designated organizer is responsible for enforcing it.
    </li>
  </ul>
</section>

<section title="Contact Information" anchor="contact">
  <ul>
    <li>
      The Computer Science Club <a href="../about/">Officers can be contacted as a whole</a>.
    </li>
  </ul>
</section>

<section title="Additional Information" anchor="info">
  <p>
    Additionally, the Executive Council are available to help Club members
    engage with local law enforcement or to otherwise help those experiencing
    unacceptable behaviour feel safe. In the context of in-person events,
    organizers will also provide escorts as desired by the person experiencing
    distress.
  </p>
  <p>
    Changes to the Code of Conduct are governed by the Club's
    <a href="constitution">constitution</a>.
  </p>
</section>

<section title="License Information And Attribution" anchor="license-attribution">
  <ul>
    <li>
      The Code of Conduct is distributed under a
      <a href="http://creativecommons.org/licenses/by-sa/3.0/">
        Creative Commons Attribution-ShareAlike License
      </a>, derived from the
      <a href="http://wics.uwaterloo.ca/code-of-conduct/">
        Women in Computer Science Code of Conduct
      </a>, the
      <a href="http://uwarc.uwaterloo.ca/policies-procedures/code-of-conduct/">
        UW Amateur Radio Club Code of Conduct
      </a>, and the
      <a href="http://fass.uwaterloo.ca/wp-content/uploads/2015/03/constitution.pdf">
        FASS Code of Conduct (Article 2, Section 16)
      </a>.
    </li>
  </ul>
</section>

<section title="Revision" anchor="revision">
  <p>Revision 1.3, adopted by the Computer Science Club of the University of Waterloo on 25 January 2018.</p>
</section>

<footer/>
</cscpage>
