#!/bin/bash

while [ ${#} != 0 ]; do
   echo "${1}:"
   cat "${1}"
   echo ""
   shift
done

exit 0

