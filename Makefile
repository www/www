FILES = events.ics events.atom news.atom index.html stats.html \
	favicon.ico default.css csclub.der csclub.pem robots.txt
SUBDIRS = about get-started events office news services media buttons logos flash library industry opencl unix102

include common.mk

common.mk: common.mk.in
	echo 'ROOT = '`pwd` > $@
	cat $< >> $@

build: noroot mkoutdir common.mk $(OUTPUTS) force
	@for i in $(SUBDIRS) ; do cd $$i && ($(MAKE) || exit 1) && cd .. ; done
	find ! -regex '.*/\..*' -type f ! -perm -0664 -print0 | xargs -r0 chmod u=rwX,g=rwX,o=rX || true
	find ! -regex '.*/\..*' -type d ! -perm 2775 -print0 | xargs -r0 chmod 2775 || true
	find ! -regex '.*/\..*' -type d ! -group www -print0 | xargs -r0 chgrp www || true

noroot:
	if test $$(id -u) = 0; then echo "don't build as root!"; exit 1; fi

$(OUTDIR)index.html: events.xml news.xml
