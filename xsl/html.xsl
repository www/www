<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">

<xsl:output method="xml" encoding="ISO-8859-1"
    doctype-public="-//W3C//DTD XHTML 1.1//EN"
    doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" />

<xsl:param name="g_date" required="yes" />
<xsl:param name="g_time" required="yes" />
<xsl:param name="g_pwd" required="yes" />
<xsl:param name="g_root" required="yes" />
<xsl:param name="g_outdir" required="yes" />

<!--

  Set global "g_pre" variable based on /cscpage/@pre, which represents the
  relative path needed to get to the root. If you do not include a "pre"
  attribute on cscpage then the "pre" attribute will default to "../".

-->
<xsl:variable name="g_pre">
 <xsl:choose>
  <xsl:when test="/cscpage/@pre"><xsl:value-of select="/cscpage/@pre" /></xsl:when>
  <xsl:otherwise>../</xsl:otherwise>
 </xsl:choose>
</xsl:variable>

<!-- mirror variables -->
<xsl:variable name="g_mirror">http://mirror.csclub.uwaterloo.ca/csclub/</xsl:variable>

<!-- include additional style sheets -->
<xsl:include href="functions.xsl" />
<xsl:include href="common.xsl" />
<xsl:include href="directory.xsl" />
<xsl:include href="menu.xsl" />
<xsl:include href="positions.xsl" />
<xsl:include href="members.xsl" />
<xsl:include href="events.xsl" />
<xsl:include href="news.xsl" />
<xsl:include href="media.xsl" />

<xsl:template match="*">
  <xsl:element name="{name()}">
    <xsl:copy-of select="@*" />
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
