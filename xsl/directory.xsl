<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="directory">
  <xsl:variable name="href">
    <xsl:choose>
      <xsl:when test="@href != ''"><xsl:value-of select="@href"/></xsl:when>
      <xsl:otherwise>.</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:if test="@title">
    <a class="diritem homedir" href="{$g_pre}">Home</a>
    <a class="diritem" href="{$href}">
      <xsl:value-of select="@title" />
    </a>
  </xsl:if>
  <xsl:apply-templates select="diritem" />
</xsl:template>

<xsl:template name="diritem">
  <xsl:param name="title" />
  <xsl:param name="href" />
  <a class="diritem" href="{$href}">
    <xsl:value-of select="$title" />
  </a>
</xsl:template>

<xsl:template match="diritem">
  <xsl:call-template name="diritem">
    <xsl:with-param name="title" select="@title" />
    <xsl:with-param name="href" select="@href" />
  </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
