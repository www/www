<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template name="events">
  <tr>
    <th colspan="2" class="news">Upcoming Events</th>
  </tr>
  <xsl:for-each select="document(concat($g_root, '/events.xml'))/eventdefs/eventitem">
    <xsl:sort select="translate(@date, '-', '')"
        order="ascending" data-type="number" />
    <xsl:if test="translate(@date, '-', '') &gt;= translate($g_date, '-', '')">
      <tr>
        <td class="eventdate"><xsl:value-of select="@date" /></td>
        <td rowspan="2" valign="top" class="eventitem">
          <a href="{translate(concat($g_pre, 'events/', @room, '-', @date, '-', @time), ' ', '_')}">
            <xsl:value-of select="@title" />
          </a>.
          <xsl:copy-of select="short" />
        </td>
      </tr>
      <tr>
        <td class="eventroom">
          <xsl:value-of select="@time" />,
          <xsl:value-of select="@room" />
        </td>
      </tr>
    </xsl:if>
  </xsl:for-each>
  <tr>
    <td class="eventitem" colspan="2">
      <a href="events/">Past events</a> are available.
    </td>
  </tr>
</xsl:template>

<xsl:template match="events-this-term">
  <xsl:call-template name="events-by-term">
    <xsl:with-param name="date" select="$g_date" />
  </xsl:call-template>
</xsl:template>

<xsl:template name="events-by-term">
  <xsl:param name="date" />
  <p>The events for <xsl:value-of select="csc:term($date)" /> are listed here.
    You can also <a href="{$g_pre}events.ics">download an ICS</a> of them.</p>
  <xsl:for-each select="document(concat($g_root, '/events.xml'))/eventdefs/eventitem">
    <xsl:sort select="translate(@date, '-', '')"
        order="ascending" data-type="number" />
    <xsl:if test="csc:term(@date) = csc:term($date)">
      <h3><a href="{translate(concat($g_pre, 'events/', @room, '-', @date, '-', @time), ' ', '_')}"><xsl:value-of select="@title" /></a></h3>
      <p>
        <strong>
          <xsl:value-of select="@room" />,
          <xsl:value-of select="@date" />,
          <xsl:value-of select="@time" />:
        </strong>
        <xsl:value-of select="short" />
      </p>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="eventdefs">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="eventitem">
  <xsl:variable name="event_translate_fix"> '</xsl:variable>
  <xsl:document method="xml" encoding="ISO-8859-1"
      doctype-public="-//W3C//DTD XHTML 1.1//EN"
      doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
      href="{translate(concat($g_outdir, @room, '-', @date, '-', @time, '.html'), $event_translate_fix, '_')}">
    <html>
      <head>
        <title><xsl:value-of select="@title" /></title>
        <link rel="stylesheet" href="{$g_pre}default.css" type="text/css" />
      </head>
      <body><div class="content">
        <xsl:call-template name="header" />
        <h2>Information</h2>
        <div>
          <p><xsl:value-of select="@title" />.</p>
          <p>
            Held in <xsl:value-of select="@room" />, on
            <xsl:value-of select="@date" />, at
            <xsl:value-of select="@time" />.
          </p>
        </div>
        <h2>Abstract</h2>
        <xsl:if test="abstract">
          <xsl:apply-templates select="abstract/node()" />
        </xsl:if>
        <xsl:if test="not(abstract)">
          <xsl:apply-templates select="short/node()" />
        </xsl:if>
        <xsl:call-template name="footer" />
      </div></body>
    </html>
  </xsl:document>
</xsl:template>

<xsl:template match="generate-event-files">
  <xsl:apply-templates select="document(concat($g_root, '/events.xml'))/eventdefs" />
</xsl:template>

<xsl:template match="old-events">
  <xsl:for-each select="document(concat($g_root, '/events.xml'))/eventdefs/eventitem">
    <xsl:sort select="translate(@date, '-', '')"
        order="descending" data-type="number" />
    <xsl:if test="not(preceding-sibling::*[csc:term(@date)=csc:term(current()/@date)]) and not(csc:term(@date) = csc:term($g_date))">
      <p><a href="{translate(concat('old-',csc:term(@date)), ' ', '_')}">
        <xsl:value-of select="csc:term(@date)" />
      </a></p>
      <xsl:document method="xml" encoding="ISO-8859-1"
            doctype-public="-//W3C//DTD XHTML 1.1//EN"
            doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
            href="{translate(concat($g_outdir, 'old-', csc:term(@date), '.html'), ' ', '_')}">
        <html>
          <head>
            <title>Events for <xsl:value-of select="csc:term(@date)" /></title>
            <link rel="stylesheet" href="{$g_pre}default.css" type="text/css" />
          </head>
          <body><div class="content">
            <xsl:call-template name="header" />
            <xsl:call-template name="events-by-term">
                <xsl:with-param name="date" select="@date" />
            </xsl:call-template>
            <xsl:call-template name="footer" />
          </div></body>
        </html>
      </xsl:document>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
