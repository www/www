<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="members-this-term">
  <p>The members for <xsl:value-of select="csc:term($g_date)" /> are
    listed here. We currently have
    <xsl:value-of select="count(csc:member-list('')/member)" />
    members. Use of this list for solicitation of any form is prohibited, if you wish to get in touch with the membership as a whole please contact <a href="mailto:exec@csclub.uwaterloo.ca">the Executive</a>.</p>
  <table>
    <tr>
      <th>Name / Webpage</th>
      <th>Program</th>
      <th>Userid</th>
    </tr>
    <xsl:for-each select="csc:member-list('')/member">
      <xsl:sort select="@name" order="ascending" />
      <xsl:variable name="class">
        <xsl:choose>
          <xsl:when test="position() mod 2 = 0">members1</xsl:when>
          <xsl:otherwise>members2</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <tr class="{$class}">
        <td><a href="/~{@userid}/"><xsl:value-of select="@name" /></a></td>
        <td><xsl:value-of select="@program" /></td>
        <td><xsl:value-of select="@userid" /></td>
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>

</xsl:stylesheet>
