<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="cscpage">
  <html>
    <head>
      <link rel="shortcut icon" href="/favicon.ico" />
      <link rel="alternate" title="CSC Events Feed" href="{$g_pre}events.atom"
          type="application/rss+xml" />
      <link rel="alternate" title="CSC News Feed" href="{$g_pre}news.atom"
          type="application/rss+xml" />
      <title><xsl:value-of select="@title" /></title>
      <link rel="stylesheet" href="{$g_pre}default.css" type="text/css" />
    </head>
    <body><div class="content">
      <a id="pagetop" />
      <xsl:apply-templates select="biglogo" />
      <xsl:apply-templates select="header" />
      <xsl:apply-templates select="section|menu" />
      <xsl:apply-templates select="footer" />
    </div></body>
  </html>
</xsl:template>

<xsl:template match="section">
  <xsl:if test="@anchor">
    <a id="{@anchor}" />
  </xsl:if>
  <h2><xsl:value-of select="@title" /></h2>
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="biglogo">
  <div class="biglogo">
    <div class="uwlogo">
      <a href="http://www.uwaterloo.ca/"><img src="{$g_pre}logos/uw_logo.png"
          alt="University of Waterloo" width="105" height="70" /></a>
    </div>
    <div class="csclogo">
      <a href="{$g_pre}"><img src="{$g_pre}logos/csc_logo.png"
          alt="Computer Science Club" width="420" height="79" /></a>
    </div>
  </div>
  <div class="dirheader">
    <xsl:apply-templates
        select="document(concat($g_pwd, '/directory.xml'))/directory"/>
  </div>
</xsl:template>

<xsl:template match="header">
  <xsl:call-template name="header" />
</xsl:template>

<xsl:template name="header">
  <xsl:param name="title" />
  <xsl:param name="href" />
  <div class="webtitle">
    <a href="{$g_pre}"><img src="{$g_pre}logos/csc_title.png"
        alt="CSC Logo" /></a>
  </div>
  <div class="dirheader">
    <xsl:apply-templates
        select="document(concat($g_pwd, '/directory.xml'))/directory"/>
    <xsl:if test="$title">
      <xsl:call-template name="diritem">
        <xsl:with-param name="title" select="$title" />
        <xsl:with-param name="href" select="$href" />
      </xsl:call-template>
    </xsl:if>
  </div>
</xsl:template>

<xsl:template match="footer" name="footer">
  <div class="footer">
    <xsl:call-template name="menu" />
    <a href="#pagetop" class="pagetop">Go to top</a>
    <div class="infobox">
      <p>
        This site supports TLS/SSL using a certificate signed by GlobalSign.
      </p><p>
        This page was generated on <xsl:value-of select="$g_date" /> at
        <xsl:value-of select="$g_time" />.
      </p><p>
        Copyright
        <xsl:text disable-output-escaping="yes">&amp;copy;</xsl:text>
        2002<xsl:text disable-output-escaping="yes">&amp;ndash;</xsl:text>2012
        Computer Science Club of the University of Waterloo.
      </p>
    </div>
  </div>
</xsl:template>

<xsl:template match="news-and-events">
  <table cellspacing="1" cellpadding="2" class="newsandevents">
    <xsl:call-template name="events" />
    <xsl:call-template name="news" />
  </table>
</xsl:template>

</xsl:stylesheet>
