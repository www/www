<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="mediafile">
  <xsl:param name="ext" />
  <xsl:param name="mirror" />
    <a href="{$mirror}{@file}{$ext}"><xsl:value-of select="@type" /></a>
    <xsl:if test="position() != last()"> | </xsl:if>
</xsl:template>

<xsl:template match="flvfile">
  <xsl:param name="mirror" />
  <script type="text/javascript" src="../flash/swfobject.js"><xsl:text> </xsl:text></script>
  <p id="player1"><a href="http://www.macromedia.com/go/getflashplayer">
    Get the Flash Player</a> to see this video using Flash Player.</p>
  <script type="text/javascript">
    var s1 = new SWFObject("../flash/flvplayer.swf","single","400","300","7");
    s1.addParam("allowfullscreen","true");
    s1.addVariable("file","<xsl:value-of select="$mirror" /><xsl:value-of select="@file" />");
    <xsl:if test="@preview">
      s1.addVariable("image","http://mirror.csclub.uwaterloo.ca/csclub/<xsl:value-of select="@preview" />");
    </xsl:if>
    s1.write("player1");
  </script>
</xsl:template>

<xsl:template match="mediaitem">
  <xsl:call-template name="makemediaitem">
    <xsl:with-param name="bittorrent">yes</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="makemediaitem">
    <xsl:with-param name="suffix">-uw</xsl:with-param>
    <xsl:with-param name="infotext">
      <p>
        If you are in residence, downloading these files will not count
        against your ResNet quota.
      </p>
    </xsl:with-param>
  </xsl:call-template>
  <xsl:variable name="thumb-url" select="concat('http://mirror.csclub.uwaterloo.ca/csclub/' ,thumbnail/@file)" />
  <li class="media">
      <a href= "{@title}">
          <span>
            <img class="media" src="{$thumb-url}" />
            <span style="display: inline-block">
                <b><xsl:value-of select="presentor" />:</b>
                <br/>
                <xsl:value-of select="@title" />
            </span>
          </span>
      </a>
  </li>
</xsl:template>

<xsl:template name="makemediaitem">
  <xsl:param name="suffix" />
  <xsl:param name="infotext" />
  <xsl:param name="bittorrent" />
  <xsl:param name="ads" />
  <xsl:variable name="realurl" select="concat('http://csclub.uwaterloo.ca/media/', csc:encode-for-uri(@title))" />
  <xsl:document method="xml" encoding="ISO-8859-1"
      doctype-public="-//W3C//DTD XHTML 1.1//EN"
      doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
      href="{concat($g_outdir, @title, $suffix, '.html')}">
    <html>
      <head>
        <title><xsl:value-of select="@title" /></title>
        <link rel="stylesheet" href="{$g_pre}default.css" type="text/css" />
      </head>
      <body><div class="content">
        <xsl:call-template name="header">
          <xsl:with-param name="title" select="@title" />
          <xsl:with-param name="href" select="concat(@title, $suffix)" />
        </xsl:call-template>
        <xsl:call-template name="donate-now" />
        <xsl:if test="$ads">
          <xsl:call-template name="inline-ads" />
        </xsl:if>
        <xsl:if test="abstract">
          <h2>Abstract</h2>
          <div>
            <xsl:apply-templates select="abstract/node()" />
          </div>
        </xsl:if>
        <xsl:if test="flvfile">
          <h2>View</h2>
        <div>
          <xsl:apply-templates select="flvfile">
            <xsl:with-param name="mirror" select="$g_mirror" />
          </xsl:apply-templates>
        </div>
        </xsl:if>
        <xsl:if test="mediafile">
          <h2>Download</h2>
          <div>
            <xsl:copy-of select="$infotext" />
            <xsl:if test="$bittorrent = 'yes'">
              <p class="mediafile">
                BitTorrent:
                <xsl:apply-templates select="mediafile">
                  <xsl:with-param name="ext">.torrent</xsl:with-param>
                  <xsl:with-param name="mirror" select="$g_mirror" />
                </xsl:apply-templates>
              </p>
            </xsl:if>
            <p class="mediafile">
              HTTP (web browser):
              <xsl:apply-templates select="mediafile">
                <xsl:with-param name="mirror" select="$g_mirror" />
              </xsl:apply-templates>
            </p>
          </div>
        </xsl:if>
        <xsl:if test="@buttons = 'yes'">
          <p>
            <!-- digg button -->
            <script type="text/javascript">
              digg_url = '<xsl:value-of select="$realurl" />';
            </script>
            <script src="http://digg.com/tools/diggthis.js"
                    type="text/javascript">
              <xsl:text> </xsl:text>
            </script>
            <!-- reddit button -->
            <script type="text/javascript">
              reddit_url = '<xsl:value-of select="$realurl" />';
              reddit_title = '<xsl:value-of select="csc:encode-for-uri(@title)" />';
            </script>
            <script src="http://reddit.com/button.js?t=3"
                    type="text/javascript">
              <xsl:text> </xsl:text>
            </script>
          </p>
        </xsl:if>
        <xsl:if test="other">
        <div>
          <xsl:apply-templates select="other/node()" />
          </div>
        </xsl:if>
        <p>
          <!-- AddThis Bookmark Button -->
          <script type="text/javascript">
            addthis_url = '<xsl:value-of select="$realurl" />';
            addthis_title = document.title;
            addthis_pub = 'calumt';
          </script>
          <script type="text/javascript"
                  src="http://s7.addthis.com/js/addthis_widget.php?v=12">
            <xsl:text> </xsl:text>
          </script>
        </p>
        <xsl:call-template name="donate-now" />
        <xsl:call-template name="footer" />
      </div></body>
    </html>
  </xsl:document>
</xsl:template>

</xsl:stylesheet>
