<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">

<xsl:output method="text" encoding="ISO-8859-1" />

<xsl:template match="//eventdefs">BEGIN:VCALENDAR
VERSION:2.0
X-WR-CALNAME:University of Waterloo Computer Science Club
PRODID:-//Apple Computer, Inc//iCal 2.0//EN
X-WR-RELCALID:3359A191-B19E-4B53-BADC-DFC084FC51C9
CALSCALE:GREGORIAN
METHOD:PUBLISH
<xsl:for-each select="eventitem"><xsl:call-template name="eventitem" /></xsl:for-each>
END:VCALENDAR
</xsl:template>

<xsl:template name="eventitem">
<xsl:variable name="room" select="@room" />
<xsl:variable name="start" select="csc:ical-datetime(@date, @time)" />
<xsl:variable name="title" select="csc:ical-escape(@title)" />
<xsl:variable name="short" select="csc:ical-escape(string(short))"  />
<xsl:variable name="abstract" select="csc:ical-escape(string(abstract))" />
BEGIN:VEVENT
LOCATION:University of Waterloo - <xsl:value-of select="$room" />
DTSTAMP:20060912T200708Z
UID:<xsl:value-of select="$start" />@csclub.uwaterloo.ca
SEQUENCE:11
DTSTART:<xsl:value-of select="$start" />
<xsl:choose>
  <xsl:when test="(string(@edate) != '') and (string(@etime) != '')">
    <xsl:variable name="end" select="csc:ical-datetime(@edate, @etime)" />
DTEND:<xsl:value-of select="$end" />
  </xsl:when>
  <xsl:when test="string(@etime) != ''">
    <xsl:variable name="end" select="csc:ical-datetime(@date, @etime)" />
DTEND:<xsl:value-of select="$end" />
  </xsl:when>
  <xsl:otherwise>
    <xsl:variable name="end" select="csc:ical-datetime(@date, @time, 60)" />
DTEND:<xsl:value-of select="$end" />
  </xsl:otherwise>
</xsl:choose>
SUMMARY:<xsl:value-of select="$title" />
DESCRIPTION:<xsl:if test="$short != ''"><xsl:value-of select="$short" /></xsl:if><xsl:if test="$short = ''"><xsl:value-of select="$abstract" /></xsl:if>
END:VEVENT
</xsl:template>

</xsl:stylesheet>
