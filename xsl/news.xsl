<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="newsdefs">
  <xsl:apply-templates select="newsitem[csc:term(@date) = csc:term($g_date)]">
    <xsl:sort select="translate(@date, '-', '')"
        order="descending" data-type="number" />
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="newsitem">
  <tr>
    <td class="newsdate"><xsl:value-of select="@date" /></td>
    <td rowspan="2" valign="top" class="newsitem"><xsl:apply-templates/></td>
  </tr>
  <tr>
    <td class="newsauthor"><xsl:value-of select="@author" /></td>
  </tr>
</xsl:template>

<xsl:template match="news" name="news">
  <tr>
    <th colspan="2" class="news">News</th>
  </tr>
  <xsl:apply-templates select="document(concat($g_root, '/news.xml'))/newsdefs" />
  <tr>
    <td class="newsitem" colspan="2">
      <a href="news/">Older news items</a> are available. <!--Make sure you check
      out our announcement board on the third floor of
      MC for more updates. -->You can also
      <a href="events.ics">download an ICS</a> of our events.
    </td>
  </tr>
</xsl:template>

<xsl:template name="news-by-term">
  <xsl:param name="date" />
  <p>The news for <xsl:value-of select="csc:term($date)" /> is listed here.</p>
  <xsl:for-each select="document(concat($g_root, '/news.xml'))/newsdefs/newsitem">
    <xsl:sort select="translate(@date, '-', '')"
        order="descending" data-type="number" />
    <xsl:if test="csc:term(@date) = csc:term($date)">
      <p>
        <strong>
          <xsl:value-of select="@date" />,
          <xsl:value-of select="@author" />:
        </strong>
        <xsl:apply-templates/>
      </p>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="old-news">
  <xsl:for-each select="document(concat($g_root, '/news.xml'))/newsdefs/newsitem">
    <xsl:sort select="translate(@date, '-', '')"
        order="descending" data-type="number" />
    <xsl:if test="not(preceding-sibling::*[csc:term(@date)=csc:term(current()/@date)]) and not(csc:term(@date) = csc:term($g_date))">
      <p><a href="{translate(concat('old-', csc:term(@date)), ' ', '_')}">
        <xsl:value-of select="csc:term(@date)" />
      </a></p>
      <xsl:document method="xml" encoding="ISO-8859-1"
          doctype-public="-//W3C//DTD XHTML 1.1//EN"
          doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
          href="{translate(concat($g_outdir, 'old-', csc:term(@date), '.html'), ' ', '_')}">
        <html>
          <head>
            <title>News for <xsl:value-of select="csc:term(@date)" /></title>
            <link rel="stylesheet" href="{$g_pre}default.css" type="text/css" />
          </head>
          <body><div class="content">
            <xsl:call-template name="header" />
            <xsl:call-template name="news-by-term">
              <xsl:with-param name="date" select="@date" />
            </xsl:call-template>
            <xsl:call-template name="footer" />
          </div></body>
        </html>
      </xsl:document>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
