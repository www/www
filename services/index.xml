<?xml version='1.0'?>
<!DOCTYPE cscpage SYSTEM "../csc.dtd">

<cscpage title="Services that the CSC offers">

 <header />
 <section title="Machine accounts">
  <p>
   The main benefit of becoming a CSC member is to get access to our various
   machines. We offer a large range of hardware, including Alpha, MIPS,
   UltraSPARC, i386, and AMD64. Our primary development machine,
   high-fructose-corn-syrup, is a 4x AMD Opteron (64 cores at 2.4 GHz)
   with 64 GiB of RAM, and it easily outperforms the Linux machines available
   through CSCF. Most of our machines are connected via gigabit ethernet.
   We offer 4 GB of disk quota that is accessible across all of our machines.
   Our wiki contains a <a
   href="https://wiki.csclub.uwaterloo.ca/wiki/Machine_List">full machine
   list</a>.

  </p><p>
   SSH key fingerprints for caffeine (our main server) can be found below:
   <div class="fixed-width">
     RSA: 0c:a3:66:52:10:19:7e:d6:9c:96:3f:60:c1:0c:d6:24<br/>
     ED25519: 9e:a8:11:bb:65:1a:31:23:38:6b:94:9d:83:fd:ba:b1
   </div>
  </p>
  <p>
   <a href="fingerprints">SSH key fingerprints for other machines</a>
  </p>
 </section>

 <section title="Email">
  <p>
   Members also receive a username@csclub.uwaterloo.ca email address.
   Mailboxes are considered as part of your disk quota, so your mailbox may
   grow up to the amount of disk quota you have. Attachments of any file size
   or type may be sent. Our mail server runs a POP3, IMAP, and SMTP server with
   SSL/TLS enabled. You can also access your mail via a <a
   href="https://mail.csclub.uwaterloo.ca">Roundcube web mail client</a>.
  </p>
 </section>

 <section title="Web space">
  <p>
   Many of members take advantage of our web hosting service. Our web server
   runs on Apache, and has PHP, Python, and Perl modules installed. We also
   have MySQL and PostgreSQL databases available upon request.
  </p><p>
   If you are already a member, you can enable your web space as follows:
  </p><ol>
   <li>
     Log in to one of the CSC machines (e.g. csclub.uwaterloo.ca)
     using an <a href="http://www.openssh.com/">SSH</a> client (e.g. <a
     href="http://www.chiark.greenend.org.uk/~sgtatham/putty/">PuTTY</a> on
     Windows or <a href="http://www.openssh.com/">OpenSSH</a> on *nix).
   </li><li>
     Create a directory called <code>www</code> in your home directory by
     typing <code>mkdir ~/www</code>. (This directory may already exist.)
   </li><li>
     Put the files you want on your web page in your new <code>www</code>
     directory. <code>index.{html,php}</code> will be loaded by default.
     You can upload files using an scp client (e.g. <a
     href="http://winscp.net/">WinSCP</a> on Windows or <a
     href="http://www.openssh.com/">OpenSSH</a> on *nix).</li>
   <li>
     Visit your snazzy new web page at
     <code>http://csclub.uwaterloo.ca/~username/</code>, where
     <code>username</code> should be replaced by your username.</li>
 </ol>
 <p>Examples of configurations for more advanced web hosting setups can be
   found in <a href="https://wiki.csclub.uwaterloo.ca/Web_Hosting">this wiki article</a>.</p>
   <p>
    If you're still having trouble getting your page up, just contact
    the <a href="mailto:webmaster@csclub.uwaterloo.ca">Webmaster</a> or the
    <a href="mailto:syscom@csclub.uwaterloo.ca">Systems Committee</a>.
  </p>
 </section>

 <section title="Club hosting">
  <p>
   If you're a club and looking for web space, the CSC is the place go. We
   offer clubs the same services we offer members (e.g. disk quota,
   databases). We also offer club.uwaterloo.ca domain registration.
  </p>
  <p>
   For more details, see the club hosting
   <a href="http://wiki.csclub.uwaterloo.ca/Club_Hosting">wiki page</a>.
   You can view a list of clubs that we have hosted or still host on our
   <a href="clubs">clubs list</a>.
  </p>
 </section>

 <section title="Library">
  <p>
   The CSC maintains an <a href="http://csclub.uwaterloo.ca/library/">
   extensive collection of Computer Science-related books</a>.
  </p>
 </section>

 <section title="Open Source Software Mirror">
   <p>
       The CSC runs a mirror of some popular open source software.
       The <a href="http://mirror.csclub.uwaterloo.ca/"> mirror </a> has a list
       of available software. More information is available on our
       <a href="http://wiki.csclub.uwaterloo.ca/Mirror">wiki article</a>.
   </p>
 </section>

 <section title="Mailing Lists">
   <p>
      In addition to running our
      <a href="http://mailman.csclub.uwaterloo.ca/listinfo/csc-general">csc-general
      mailing list</a> to inform members about our current events, we also run
      a
      <a href="http://mailman.csclub.uwaterloo.ca/listinfo/csc-industry">csc-industry
      mailing list</a>,
      which allows students to opt-in to receiving emails from industry
      representatives. Corporate events, job postings, info sessions, and the
      like may be posted here.
   </p>
   <p>
      If you're interested in getting more involved with the Computer Science
      Club, <a href="mailto:exec@csclub.uwaterloo.ca">email the executive</a>
      to ask about joining our programme committee. It has its own mailing
      list that members can also join.
   </p>
 </section>

 <section title="IRC">
  <p>
    We host an instance of <a href="https://chat.csclub.uwaterloo.ca">The Lounge</a>
    for all of our members. The Lounge is a web-based IRC client which is simple to
    setup and use. It also has a Progressive Web App available for mobile devices.
  </p>
  <p>
    We also host an instance of <a href="https://znc.csclub.uwaterloo.ca">ZNC</a>
    for those who wish to use their own IRC client but do not want to run their own
    bouncer. ZNC saves your messages persistently so you do not have to keep your
    IRC client running 24/7.
  </p>
  <p>
    For more information, see <a href="https://wiki.csclub.uwaterloo.ca/How_to_IRC">
    this page</a> on our wiki.
  </p>
 </section>

 <section title="Video Conferencing">
  <p>
    We host an instance of <a href="https://bbb.csclub.uwaterloo.ca">BigBlueButton</a>,
    a free and open-source video conferencing platform. BigBlueButton offers many
    useful features such as a multi-user whiteboard, breakout rooms, shared notes,
    and more.
  </p>
  <p>
    To get the most out of BigBlueButton, you can watch some tutorial videos
    <a href="https://bigbluebutton.org/html5">here</a>.
  </p>
 </section>

 <section title="Live Streaming">
  <p>
    We host an instance of <a href="https://icy.csclub.uwaterloo.ca">Icecast</a>,
    which can stream live audio and video. We have successfully streamed live
    events to Icecast using OBS Studio. Latency usually ranges between 5-10 sec.
  </p>
  <p>
    If you are interested in live streaming via Icecast, please contact the
    Systems Committee (syscom at csclub dot uwaterloo dot ca).
  </p>
 </section>

 <footer />
</cscpage>
